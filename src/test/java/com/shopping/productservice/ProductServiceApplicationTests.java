package com.shopping.productservice;

import com.shopping.productservice.controller.ProductController;
import com.shopping.productservice.dto.ProductResponse;
import com.shopping.productservice.exception.ProductExceptionHandler;
import com.shopping.productservice.exception.ProductNotFoundException;
import com.shopping.productservice.model.Product;
import com.shopping.productservice.repository.ProductRepository;
import com.shopping.productservice.service.ProductService;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.migrationsupport.rules.EnableRuleMigrationSupport;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
@EnableRuleMigrationSupport
class ProductServiceApplicationTests {

	private MockMvc mockmvc;

	@Mock
	private ProductRepository productRepository;

	private ProductController productController;
	private ProductService productService;
	private ProductResponse productResponse;
	private Product product;

	@BeforeEach
	void init() {
		productService = new ProductService(productRepository);
		productController = new ProductController(productService);
		mockmvc = MockMvcBuilders.standaloneSetup(productController)
				.setControllerAdvice(new ProductExceptionHandler())
				.build();

		productResponse = ProductResponse.builder()
				.name("testing name")
				.description("testing description")
				.price(BigDecimal.valueOf(100))
				.build();

		product = Product.builder()
				.id(1L)
				.name("testing name")
				.description("testing description")
				.price(BigDecimal.valueOf(100))
				.build();
	}

	@Test
	void shouldGetOneProduct() throws Exception {
		long id = 1;

		Mockito.when(productRepository.findById(id)).thenReturn(Optional.of(product));
		mockmvc.perform(MockMvcRequestBuilders.get("/api/product/{id}",id))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.name").value(productResponse.getName()));
	}

	@Test
	void shouldThrowExceptionWhenProductNotFound() throws Exception {
		long id = 1;
		Mockito.when(productRepository.findById(id)).thenReturn(Optional.empty());

		mockmvc.perform(MockMvcRequestBuilders.get("/api/product/{id}",id))
				.andExpect(MockMvcResultMatchers.status().isNotFound())
				.andExpect(result -> assertInstanceOf(ProductNotFoundException.class, result.getResolvedException()));
	}

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void shouldThrowExceptionWhenProductNotFoundUsingRule() throws Exception {
		long id = 1;
		Mockito.when(productRepository.findById(id)).thenReturn(Optional.empty());
		thrown.expect(ProductNotFoundException.class);
		productService.getOneProduct(id);
	}
}

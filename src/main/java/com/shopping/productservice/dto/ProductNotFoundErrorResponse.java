package com.shopping.productservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductNotFoundErrorResponse {
    private int status;
    private String message;
    private Long timestamp;
}



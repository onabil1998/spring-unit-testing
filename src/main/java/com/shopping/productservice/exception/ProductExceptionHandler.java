package com.shopping.productservice.exception;

import com.shopping.productservice.dto.ProductNotFoundErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ProductExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ProductNotFoundErrorResponse> handleProductNotFoundException(ProductNotFoundException exc) {

        ProductNotFoundErrorResponse error = new ProductNotFoundErrorResponse();
        error.setMessage(exc.getMessage());
        error.setTimestamp(System.currentTimeMillis());
        error.setStatus(HttpStatus.NOT_FOUND.value());

        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}

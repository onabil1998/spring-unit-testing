package com.shopping.productservice.controller;

import com.shopping.productservice.dto.ProductNotFoundErrorResponse;
import com.shopping.productservice.dto.ProductRequest;
import com.shopping.productservice.dto.ProductResponse;
import com.shopping.productservice.exception.ProductNotFoundException;
import com.shopping.productservice.model.Product;
import com.shopping.productservice.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createProduct(@RequestBody ProductRequest productRequest) {
        productService.createProduct(productRequest);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProductResponse> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping("/{studentId}")
    @ResponseStatus(HttpStatus.OK)
    public ProductResponse getOneProduct(@PathVariable Long studentId) {
        return productService.getOneProduct(studentId);
    }

}
